#Область ОбработчикиСобытий
Процедура ОбработкаПроведения(Отказ, Режим)

	// регистр Начисления
	Движения.Начисления.Записывать = Истина;
	Для Каждого ТекСтрокаНачисления Из Начисления Цикл
		Движение = Движения.Начисления.Добавить();
		Движение.Сторно = Ложь;
		Движение.ВидРасчета = ПредопределенноеЗначение("ПланВидовРасчета.Начисления.НевыходСотрудника");
		Движение.ПериодРегистрации = ТекСтрокаНачисления.ДатаНачала;
		Движение.ПериодДействияНачало = ТекСтрокаНачисления.ДатаНачала;
		Движение.ПериодДействияКонец = КонецДня(ТекСтрокаНачисления.ДатаОкончания);
		Движение.Сотрудник = ТекСтрокаНачисления.Сотрудник;
	КонецЦикла;

КонецПроцедуры
#КонецОбласти
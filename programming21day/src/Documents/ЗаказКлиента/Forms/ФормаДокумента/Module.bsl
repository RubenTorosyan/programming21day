#Область ОбработчикиСобытийЭлементовТаблицыФормыТовары
&НаКлиенте
Процедура ТоварыКоличествоПриИзменении(Элемент)
	ОбщийМодульКлиент.РасчетСуммуПоСтроке(Элементы.Товары.ТекущиеДанные);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыЦенаПриИзменении(Элемент)
	ОбщийМодульКлиент.РасчетСуммуПоСтроке(Элементы.Товары.ТекущиеДанные);
КонецПроцедуры

&НаКлиенте
Процедура ТоварыСуммаПриИзменении(Элемент)
	
	ОбщийМодульКлиент.РасчетСуммуПоСтроке(Элементы.Товары.ТекущиеДанные, Ложь);
	
КонецПроцедуры

#КонецОбласти